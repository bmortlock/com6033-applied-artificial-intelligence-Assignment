#This code is based on a gitbub project available at https://github.com/akashdeepjassal/mnist-flask
from flask import Flask, render_template, request
from imageio import imread, imsave
from skimage.transform import resize
import numpy as np
import keras.models
import re
import base64
import pickle
import joblib
import flask
import pandas as pd


import sys 
import os
# append the model path
sys.path.append(os.path.abspath("./model"))

app = Flask(__name__)

with open("./model/model.joblib", "rb") as f:
    model = joblib.load(f)
    
@app.route('/')
def index():
    return render_template("index.html")


@app.route('/predict',methods=['POST'])
def predict():
    #For rendering results on HTML GUI
	

		int_features = [float(x) for x in request.form.values()]
		final_features = [np.array(int_features)]
		prediction = model.predict(final_features)
		output = round(prediction[0], 2) 
		return render_template('index.html', prediction_text='The genre of a publishers game based on this data would be  :{}'.format(output))
	
@app.route('/results',methods=['POST'])
def results():

    data = request.get_json(force=True)
    prediction = model.predict([np.array(list(data.values()))])

    output = prediction[0]
    return jsonify(output)


	

if __name__ == '__main__':
	app.run(debug=True)